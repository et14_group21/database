DROP DATABASE IF EXISTS `timer`;
CREATE DATABASE `timer`;

CREATE TABLE `timer`.`dates` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `start` DATETIME NULL,
  `end` DATETIME NULL,
  `consumer` INT NULL,
  `comment` VARCHAR(50) NULL,
  `type` INT NULL,
  `repeat` INT NULL,
  PRIMARY KEY (`ID`));

  CREATE TABLE `timer`.`error` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  `type` INT NULL,
  `name` VARCHAR(50) NULL,
  `code` INT NULL,
  PRIMARY KEY (`ID`));

CREATE USER 'timerUser'@'localhost' IDENTIFIED BY 'group21';
GRANT DELETE ON timer.* TO 'timerUser'@'localhost';
GRANT INSERT ON timer.* TO 'timerUser'@'localhost';
GRANT SELECT ON timer.* TO 'timerUser'@'localhost';
GRANT UPDATE ON timer.* TO 'timerUser'@'localhost';
